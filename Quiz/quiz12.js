/* buatlah function 
luas lingkaran :  3.14 * r2
luas segitiga : 1/2 a*t
luas persegi : pxl

tampilkan panggil functionnya dengan console.log
lengkap dengan mengisi parameternya

*/
console.log('luas lingkaran')
function lingkaran(a,b,c) {
    return a * b * c
}

var num1 = 3.14
var n = num1.toFixed(2);
var num2 = 5
var num3 = 5

var luasLingkaran = lingkaran(n,num2,num3)
console.log(luasLingkaran)
console.log

console.log('luas segitiga')
function segitiga(a,b,c) {
    return a * b * c
}

var num1 = 0.5
var n = num1.toFixed(2);
var num2 = 5
var num3 = 2

var luasSegitiga = segitiga(n,num2,num3)
console.log(luasSegitiga)
console.log

console.log('luas persegi')
function persegi(a,b) {
    return a * b 
}

var num1 = 7
var num2 = 7

var luasPersegi = persegi(num1,num2)
console.log(luasPersegi)
console.log