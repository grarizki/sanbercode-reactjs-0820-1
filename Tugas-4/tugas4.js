//Soal 1
console.log("LOOP PERTAMA");

var nilai = 0;
while (nilai <20) {
  console.log(nilai + ' - I Love Coding')
  nilai = nilai + 2;
}
console.log('LOOP KEDUA');

var nilai = 20;
while (nilai >0) {
  console.log(nilai + ' I will become a frontend developer')
  nilai = nilai - 2;
}

//Soal 2
for(var i = 1; i <= 20; i++) {
  if (i % 3 === 0 && i % 2 === 1){
    console.debug(i + " - I Love Coding")
  }else if(i % 2 === 1){
    console.debug(i + " - Santai")
  }else{
    console.debug(i + " - Berkualitas")
  }
}   
console.log()

//Soal 3
for (var i = '0'; i <= 7; i++) {
  var x = "";
  for(var j = 1; j <= i; j++) {
    x = x + '#';
  }
console.log(x);
}
console.log()

//Soal 4
var kalimat = "saya sangat senang belajar javascript";
var x = kalimat.split(' ')
console.log(x)

//Soal 5 
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort()
console.log(daftarBuah)