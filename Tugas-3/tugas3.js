/* soal 1 mengurutkan menjadi seperti saya Senang belajar JAVASCRIPT */
var kataPertama = "saya ";
var kataKedua = "senang ";
var kataKetiga = "belajar ";
var kataKeempat = "javascript "; 
var gabungan = kataPertama + (kataKedua[0].toUpperCase() + kataKedua.slice(1)) + kataKetiga.toLowerCase() + kataKeempat.toUpperCase();
// jawab 1
console.log(kataPertama);
//jawab 2
console.log(kataKedua[0].toUpperCase() + kataKedua.slice(1));
//jawab 3
console.log(kataKetiga.toLowerCase());
// jawab 4
var upper = kataKeempat.toUpperCase();
console.log(upper);
console.log(gabungan);

/* soal 2 mengurutkan menjadi seperti diubah menjadi integer lalu jumlahkan dan tampilkan */
 var kataPertama = "1 ";
 var kataKedua = "2 ";
 var kataKetiga = "4 ";
 var kataKeempat = "5 ";

/* jawab */
var a = parseInt(kataPertama);
var b = parseInt(kataKedua);
var c = parseInt(kataKetiga);
var d = parseInt(kataKeempat);
console.log(a + b + c + d);

/* soal 3 */
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); // do your own! 
var kataKetiga = kalimat.substring(15, 18); // do your own! 
var kataKeempat = kalimat.substring(19, 24); // do your own! 
var kataKelima = kalimat.substring(25, 31); // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

//soal 4 
var nilai = 75 ;
//jawab
if (nilai >= 80 ) {
    console.log(nilai + ' indeksnya A');
} else if (nilai >= 70 && nilai < 80 ) {
    console.log(nilai + ' indeksnya B');
} else if (nilai >= 60 && nilai < 70 ) {
    console.log(nilai + ' indeksnya C');
} else if (nilai >= 50 && nilai < 60 ) {
    console.log(nilai + ' indeksnya D');
} else if(nilai < 50) {
    console.log(nilai + ' indeksnya E'); }


//soal 5 
var tanggal = 1;
var bulan = 11;
var tahun = 1996;
//jawab switch case bulan
switch (bulan) {
    case 1: {console.log('November'); break;}
    default: { console.log ('1 November 1996');}
}
